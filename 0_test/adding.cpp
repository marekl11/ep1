#include <iostream>
#include <string>
#include <algorithm>

int main()
{
    std::ios::sync_with_stdio(false);

    int N;
    std::cin >> N;

    std::string no1;
    std::string no2;
    std::string result;
    for (int i = 0; i < N; ++i)
    {
        std::cin >> no1 >> no2;
        
        std::reverse(no1.begin(), no1.end());
        std::reverse(no2.begin(), no2.end());

        int iNo1 = std::stoi(no1);
        int iNo2 = std::stoi(no2);

        result = std::to_string(iNo1 + iNo2);

        std::reverse(result.begin(), result.end());

        std::cout << std::stoi(result) << std::endl;
    }

    return 0;
}