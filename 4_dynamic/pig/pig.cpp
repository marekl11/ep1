#include <iostream>
#include <map>
#include <unordered_map>
#include <unordered_set>

class CpigSolver
{
public:
    CpigSolver() : minimalValue(INT32_MAX) {}

    /**
     * @brief Input Specification
                The input consists of T test cases. The number of them (T) is given on the first line of the input file.
                Each test case begins with a line containing two integers E and F. They indicate the weight of an
                empty pig and of the pig filled with coins. Both weights are given in grams. No pig will weigh more
                than 10 kg, that means 1 ≤ E ≤ F ≤ 10000. On the second line of each test case, there is an integer
                number N (1 ≤ N ≤ 500) that gives the number of various coins used in the given currency. Following
                this are exactly N lines, each specifying one coin type. These lines contain two integers each, P and
                W (1 ≤ P ≤ 50000, 1 ≤ W ≤ 10000). P is the value of the coin in monetary units, W is it’s weight
                in grams.
     * 
     */
    void readInput()
    {
        int T; //number of test cases
        std::cin >> T;

        for (int testCase = 0; testCase < T; ++testCase)
        {
            int E, F; //starting weight, end weight
            std::cin >> E >> F;
            coinsTotalWeight = F - E;
            int N; //number of coin types
            std::cin >> N;
            for (int coinIndex = 0; coinIndex < N; ++coinIndex)
            {
                int P, W;
                std::cin >> P >> W; //P, W
                coins[P] = W;
            }
            solve(coinsTotalWeight, 0);
            if (minimalValue != INT32_MAX)
            {
                std::cout << "The minimum amount of money in the piggy-bank is " << minimalValue << '.' << std::endl;
            }
            else
            {
                std::cout << "This is impossible." << std::endl;
            }
            cleanup();
        }
    }

    /**
     * @brief Recursive function intendet for solving the piggy bank problem
     * 
     * @param weight weight of the bank
     * @param value current known value of the bank
     */

    void solve(int weight, int value)
    {
        if (weight < 0)
        {
            return;
        }
        if (weight == 0)
        {
            if (value < minimalValue)
            {
                minimalValue = value;
            }
            return;
        }

        for (auto it = coins.begin(); it != coins.end(); ++it)
        {
            auto weightIt = searchTable.find(weight - it->second);
            if (weightIt != searchTable.end())
            {
                auto valueIt = weightIt->second.find(value + it->first);
                if (valueIt != weightIt->second.end())
                {
                    continue;
                }
            }
            searchTable[weight - it->second].emplace(value + it->first);
            solve(weight - it->second, value + it->first);
        }
    }

    /**
     * @brief frees all memory
     * 
     */

    void cleanup()
    {
        coins.clear();
        coinsTotalWeight = 0;
        minimalValue = INT32_MAX;
        searchTable.clear();
    }

private:
    //value, weight
    std::map<int, int> coins;
    int coinsTotalWeight;
    int minimalValue;

    //weight, value
    std::unordered_map<int, std::unordered_set<int>> searchTable;
};

int main()
{
    std::ios::sync_with_stdio(false);

    CpigSolver pigSolver;

    pigSolver.readInput();

    return 0;
}