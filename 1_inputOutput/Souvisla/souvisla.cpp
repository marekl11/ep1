#include <iostream>

int main()
{
    std::ios::sync_with_stdio(false);

    int rounds;
    std::cin >> rounds;

    unsigned int newNumber;
    unsigned int oldNumber;
    for (int i = 0; i < rounds; ++i)
    {
        unsigned int counter = 1;
        unsigned int counterGOAT = 0;
        std::cin >> newNumber;
        while (true)
        {
            if (newNumber == 0)
            {
                break;
            }

            oldNumber = newNumber;
            std::cin >> newNumber;

            if (newNumber >= oldNumber)
            {
                ++counter;
            }
            else
            {
                if (counter > counterGOAT)
                    counterGOAT = counter;
                counter = 1;
            }
        }

        std::cout << counterGOAT << std::endl;
    }

    return 0;
}