#include <iostream>
#include <string>
#include <stack>
#include <cassert>

char invert(const char character)
{
    if (character == ')')
    {
        return '(';
    }
    else if (character == ']')
    {
        return '[';
    }
    else if (character == '}')
    {
        return '{';
    }
    else if (character == '>')
    {
        return '<';
    }

    return 'c';
}

bool checkLine(const std::string &line)
{
    std::stack<char> stack;
    for (size_t i = 0; i < line.size(); ++i)
    {
        //check
        if (line[i] == '*' && i + 1 < line.size() && line[i + 1] == ')')
        {
            if (stack.empty() == true)
            {
                return false;
            }
            char star = stack.top();
            stack.pop();
            if (stack.empty() == true)
            {
                return false;
            }
            char round = stack.top();
            stack.pop();

            if (star != '*' || round != '(')
            {
                return false;
            }
            else
            {
                ++i;
            }
        }
        else if (line[i] == ')' || line[i] == ']' || line[i] == '}' || line[i] == '>')
        {
            if (stack.empty() == true)
            {
                return false;
            }
            if (stack.top() == invert(line[i]))
            {
                stack.pop();
            }
            else
            {
                return false;
            }
        }

        //read
        else if (line[i] == '(' && i + 1 < line.size() && line[i + 1] == '*')
        {
            stack.emplace(line[i]);
            stack.emplace(line[i + 1]);
            ++i;
        }
        else if (line[i] == '(' || line[i] == '[' || line[i] == '{' || line[i] == '<')
        {
            stack.emplace(line[i]);
        }
    }

    //has to be naked
    if (stack.empty() == false)
    {
        return false;
    }

    //everything is ok
    return true;
}

int main()
{
    assert(checkLine("(())"));
    assert(checkLine("{()}"));
    assert(checkLine("aa((aa)aa)"));
    assert(checkLine("(*()*)"));
    assert(checkLine("({{}})"));
    assert(checkLine("*(())***"));
    assert(checkLine("[]"));
    assert(checkLine("rfhrfdb<*>"));
    assert(checkLine("([**])"));
    assert(checkLine("0"));
    assert(checkLine(""));
    assert(checkLine("(.)(.)"));
    assert(checkLine("([*  *])"));
    assert(checkLine("-([3+(*y*)$])/"));
    assert(checkLine("-([3+(*y*)$])/"));
    assert(checkLine("(*((*(((([((()))]))))*))*)"));
    assert(checkLine("(*(aaaa(*(tfhtj((([yjtf((tyjtyjy(j))aaaaaaa)]drffyjgrhytf))))*))ytjyrfytthf*)"));
    assert(checkLine("*(**)*"));
    assert(checkLine("(*(a*aa*a(*(tfh*tj((([yj*tf((tyj*tyjy(j))aaaaaaa)]dr**ffyj*(**)*grhytf))))*))ytjy***rfytthf*)"));
    assert(checkLine("*f*erg*rgr**rhdth***rhtdhr*rthjty**rfj"));
    assert(checkLine("*f*erg*(rgr**rh)dth***rhtdhr*rthjty**rfj"));

    assert(!checkLine("(*)"));
    assert(!checkLine("(()"));
    assert(!checkLine("([*"));
    assert(!checkLine("("));
    assert(!checkLine("(*"));
    assert(!checkLine("*)"));
    assert(!checkLine("(*[*)"));
    assert(!checkLine("(*[*))"));
    assert(!checkLine("(*{*})"));
    assert(!checkLine("*)()"));
    assert(!checkLine(")("));
    assert(!checkLine("*)"));
    assert(!checkLine("(*"));

    assert(invert(')') == '(');
    assert(invert(']') == '[');
    assert(invert('}') == '{');
    assert(invert('>') == '<');

    std::ios::sync_with_stdio(false);

    int N;
    std::cin >> N;
    std::cin.ignore();
    std::string line;
    for (int i = 0; i < N; ++i)
    {
        getline(std::cin, line);

        //std::cout << line << std::endl;

        if (checkLine(line) == true)
        {
            std::cout << "Plan je v poradku." << std::endl;
        }
        else
        {
            std::cout << "V planu je chyba." << std::endl;
        }
    }

    return 0;
}