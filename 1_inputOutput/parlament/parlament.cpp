#include <iostream>

#define MAXSIZE 1001

void preprocess(int **parlament)
{
    for (int i = 1; i < MAXSIZE; ++i)
    {
        for (int j = 1; j < MAXSIZE; ++j)
        {
            parlament[i][j] = parlament[i - 1][j] + parlament[i][j - 1] - parlament[i - 1][j - 1] + parlament[i][j];
        }
    }
}

void answer(int **parlament, int R1, int S1, int R2, int S2)
{
    int value = parlament[R2][S2] - parlament[R1 - 1][S2] - parlament[R2][S1 - 1] + parlament[R1 - 1][S1 - 1];
    std::cout << "Absolutni hodnota pohodlnosti je " << value << " bodu." << std::endl;
}

int main()
{
    std::ios::sync_with_stdio(false);

    int N;
    std::cin >> N;

    //allocate parlament
    int **parlament = new int *[MAXSIZE];
    for (int i = 0; i < MAXSIZE; ++i)
    {
        parlament[i] = new int[MAXSIZE];
    }
    //initialize 0 borders
    for (int i = 0; i < MAXSIZE; ++i)
    {
        parlament[0][i] = 0;
    }
    for (int i = 0; i < MAXSIZE; ++i)
    {
        parlament[i][0] = 0;
    }

    for (int task = 0; task < N; ++task)
    {
        //read parlament
        int R, S;
        std::cin >> R >> S;

        for (int row = 0; row < R; ++row)
        {
            for (int seat = 0; seat < S; ++seat)
            {
                std::cin >> parlament[row + 1][seat + 1];
            }
        }

        //preprocess parlament
        preprocess(parlament);

        //read task descriptions
        int D;
        std::cin >> D;
        for (int description = 0; description < D; ++description)
        {
            int R1, S1, R2, S2;
            std::cin >> R1 >> S1 >> R2 >> S2;
            answer(parlament, R1, S1, R2, S2);
        }

        std::cout << std::endl;
    }

    return 0;
}