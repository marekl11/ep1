#include <iostream>
#include <cmath>
#include <map>

double powerOfTwoDistance(double x1, double y1, double x2 , double y2)
{
    return pow(x2 - x1, 2) + pow(y2 - y1, 2);
}

struct doubleCmp
{
    bool operator()(const double a, const double b) const
    {
        double epsilon = 1.0E-8f;
        return (abs(a - b) > epsilon) && (a < b);
    }
};

int main()
{
    std::map<double, int, doubleCmp> lenghts;
    while (true)
    {
        //get number of lines
        double numberOfLines;
        std::cin >> numberOfLines;

        if (numberOfLines == 0)
        {
            break;
        }

        //calculate lenghts
        for (int i = 0; i < numberOfLines; ++i)
        {
            double x1, y1, x2, y2;
            std::cin >> x1 >> y1 >> x2 >> y2;
            double lenght = powerOfTwoDistance(x1, y1, x2, y2);
            auto it = lenghts.find(lenght);
            if (it == lenghts.end())
            {
                lenghts[lenght] = 1;
            }
            else
            {
                ++(it->second);
            }
        }

        //find
        int value = 0;
        double key = 0;
        for (auto it = lenghts.begin(); it != lenghts.end(); ++it)
        {
            if (value < it->second)
            {
                value = it->second;
                key = it->first;
            }
        }

        //print
        std::cout.precision(2);
        std::cout << value << " usecek ma delku " << std::fixed << sqrt(key) << '.' << std::endl;
    }

    return 0;
}