#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

double powerOfTwoDistance(double x1, double y1, double x2, double y2)
{
    return pow(x2 - x1, 2) + pow(y2 - y1, 2);
}

bool doubleCmp(const double a, const double b)
{
    return abs(a - b) < 1.0E-8f;
}

int main()
{
    std::ios::sync_with_stdio(false);

    std::vector<double> lenghts;
    while (true)
    {
        //get number of lines
        double numberOfLines;
        std::cin >> numberOfLines;

        if (numberOfLines == 0)
        {
            break;
        }

        //calculate lenghts
        for (int i = 0; i < numberOfLines; ++i)
        {
            double x1, y1, x2, y2;
            std::cin >> x1 >> y1 >> x2 >> y2;
            lenghts.emplace_back(powerOfTwoDistance(x1, y1, x2, y2));
        }

        //find
        std::sort(lenghts.rbegin(), lenghts.rend());

        double longest = lenghts[0];
        int counter = 0;
        for (size_t i = 0; i < lenghts.size(); i++)
        {
            if (doubleCmp(longest, lenghts[i]) == true)
            {
                ++counter;
            }
        }

        //print
        std::cout.precision(2);
        std::cout << counter << " usecek ma delku " << std::fixed << sqrt(longest) << '.' << std::endl;
        lenghts.clear();
    }

    return 0;
}