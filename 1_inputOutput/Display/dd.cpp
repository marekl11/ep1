#include <iostream>
#include <string>

#define ROWS 7
#define COLUMNS 5

static const char zero[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'+', ' ', ' ', ' ', '+'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'+', '-', '-', '-', '+'}};

static const char one[ROWS][COLUMNS] = {{' ', ' ', ' ', ' ', '+'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {' ', ' ', ' ', ' ', '+'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {' ', ' ', ' ', ' ', '+'}};

static const char two[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {' ', ' ', ' ', ' ', '|'},
                                        {'+', '-', '-', '-', '+'},
                                        {'|', ' ', ' ', ' ', ' '},
                                        {'|', ' ', ' ', ' ', ' '},
                                        {'+', '-', '-', '-', '+'}};

static const char three[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {'+', '-', '-', '-', '+'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {'+', '-', '-', '-', '+'}};

static const char four[ROWS][COLUMNS] = {{'+', ' ', ' ', ' ', '+'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'+', '-', '-', '-', '+'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {' ', ' ', ' ', ' ', '+'}};

static const char five[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                         {'|', ' ', ' ', ' ', ' '},
                                         {'|', ' ', ' ', ' ', ' '},
                                         {'+', '-', '-', '-', '+'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {'+', '-', '-', '-', '+'}};

static const char six[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                        {'|', ' ', ' ', ' ', ' '},
                                        {'|', ' ', ' ', ' ', ' '},
                                        {'+', '-', '-', '-', '+'},
                                        {'|', ' ', ' ', ' ', '|'},
                                        {'|', ' ', ' ', ' ', '|'},
                                        {'+', '-', '-', '-', '+'}};

static const char seven[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '+'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '|'},
                                          {' ', ' ', ' ', ' ', '+'}};

static const char eight[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                          {'|', ' ', ' ', ' ', '|'},
                                          {'|', ' ', ' ', ' ', '|'},
                                          {'+', '-', '-', '-', '+'},
                                          {'|', ' ', ' ', ' ', '|'},
                                          {'|', ' ', ' ', ' ', '|'},
                                          {'+', '-', '-', '-', '+'}};

static const char nine[ROWS][COLUMNS] = {{'+', '-', '-', '-', '+'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'|', ' ', ' ', ' ', '|'},
                                         {'+', '-', '-', '-', '+'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {' ', ' ', ' ', ' ', '|'},
                                         {'+', '-', '-', '-', '+'}};

static const char *ASCIINumbers[10] = {&zero[0][0], &one[0][0], &two[0][0], &three[0][0], &four[0][0], &five[0][0], &six[0][0], &seven[0][0], &eight[0][0], &nine[0][0]};

int *getNumbers(const std::string &input)
{
    static int numbers[4];

    int j = 0;
    for (int i = 0; i < COLUMNS; ++i)
    {
        if (input[i] == ':')
            continue;

        numbers[j] = input[i] - '0';
        ++j;
    }

    return numbers;
}

void printOutput(const int numbers[4])
{
    for (int row = 0; row < ROWS; ++row)
    {
        for (int number = 0; number < 4; ++number)
        {
            for (int column = 0; column < COLUMNS; ++column)
            {
                std::cout << *(ASCIINumbers[numbers[number]] + column + row * COLUMNS);
            }

            if (number != 3)
                std::cout << "  ";

            if (number == 1)
            {
                if (row == 2 || row == 4)
                {
                    std::cout << "o  ";
                }
                else
                {
                    std::cout << "   ";
                }
            }
        }
        std::cout << '\n';
    }

    std::cout << '\n'
              << std::endl;
}

int main()
{
    std::ios::sync_with_stdio(false);

    std::string input;

    while (true)
    {
        getline(std::cin, input);

        if (input == "end")
        {
            std::cout << "end" << std::endl;
            break;
        }

        printOutput(getNumbers(input));
    }

    return 0;
}