#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <algorithm>

class CanagramMaker
{
public:
    CanagramMaker()
    {
        m_word.reserve(15);
        m_boolArray.reserve(15);
        m_anagram.reserve(10);
    }

    bool loadInput()
    {
        std::cin >> m_desiredLenght;
        if (m_desiredLenght == 0)
        {
            return false;
        }
        std::cin >> m_word;
        sort(m_word.begin(), m_word.end());

        return true;
    }

    void makeAnagrams()
    {
        //reset
        m_boolArray.assign(m_word.length(), true);
        counter = 0;

        recursion(m_anagram, m_boolArray);
        std::cout << counter << std::endl;
    }

private:
    std::string m_word;
    size_t m_desiredLenght;
    std::vector<bool> m_boolArray;
    std::vector<bool> lastBoolArray;
    std::string m_anagram;
    int counter;

    void recursion(std::string anagram, std::vector<bool> boolArray)
    {
        if (anagram.length() == m_desiredLenght)
        {
            ++counter;
            std::cout << anagram << std::endl;
            return;
        }

        lastBoolArray.assign(m_word.size(), false);
        for (size_t i = 0; i < m_word.length(); ++i)
        {
            if (boolArray[i] == false)
            {
                continue;
            }

            boolArray[i] = false;
            if (compareSets(lastBoolArray, boolArray) == true && anagram.length() != m_word.length() - 1)
            {
                boolArray[i] = true;
                continue;
            }
            anagram.push_back(m_word[i]);
            recursion(anagram, boolArray);
            lastBoolArray = boolArray;
            boolArray[i] = true;
            anagram.erase(anagram.begin() + anagram.size() - 1);
        }
    }

    bool compareSets(std::vector<bool> boolArray1, std::vector<bool> boolArray2)
    {
        int offset1 = 0;
        int offset2 = 0;

        for (size_t i = 0; i < m_word.size(); ++i)
        {

            if (boolArray1[i + offset1] == false)
            {
                for (size_t j = i + offset1; j < m_word.size(); ++j)
                {
                    if (boolArray1[j] == false)
                    {
                        ++offset1;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if (boolArray2[i + offset2] == false)
            {
                for (size_t j = i + offset2; j < m_word.size(); ++j)
                {
                    if (boolArray2[j] == false)
                    {
                        ++offset2;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (m_word[i + offset1] != m_word[i + offset2])
            {
                return false;
            }
            else if (i + offset1 == m_word.size() - 1 || i + offset2 == m_word.size() - 1)
            {
                return true;
            }
        }
        return true;
    }
};

int main()
{
    std::ios::sync_with_stdio(false);

    CanagramMaker anagramMaker;
    while (anagramMaker.loadInput() == true)
    {
        anagramMaker.makeAnagrams();
    }

    return 0;
}