#include <iostream>
#include <string>
#include <stack>

class CnotationConverter
{
public:
    CnotationConverter()
    {
        prefixExpression.reserve(398);
        postfixExpression.reserve(398);
    }

    bool readInput()
    {
        getline(std::cin, prefixExpression);
        if (prefixExpression == "0")
        {
            return false;
        }
        removeSpaces(prefixExpression);

        return true;
    }

    void convert()
    {
        reset();

        reorganize(0);

        printExpression(postfixExpression);
    }

private:
    std::string prefixExpression;
    std::string postfixExpression;

    void removeSpaces(std::string &expression)
    {
        std::string expressionWithoutSpaces;
        expressionWithoutSpaces.reserve(199);
        for (size_t i = 0; i < expression.size(); ++i)
        {
            if (expression[i] == ' ')
            {
                continue;
            }
            else
            {
                expressionWithoutSpaces.push_back(expression[i]);
            }
        }
        expression = expressionWithoutSpaces;
    }

    void reset()
    {
        postfixExpression.clear();
    }

    int reorganize(int index)
    {
        int first;
        int second;
        if (isNumber(prefixExpression[index]) == true)
        {
            postfixExpression.push_back(prefixExpression[index]);
            return 1;
        }
        else
        {
            first = reorganize(index + 1);
            second = reorganize(index + first + 1);
        }
        postfixExpression.push_back(prefixExpression[index]);
        return first + second + 1;
    }

    bool isNumber(const char character)
    {
        if (character >= '0' && character <= '9')
        {
            return true;
        }
        return false;
    }

    void printExpression(const std::string &expression)
    {
        int lastIndex = expression.size() - 1;
        for (int i = 0; i < lastIndex; ++i)
        {
            std::cout << expression[i] << ' ';
        }
        std::cout << expression[lastIndex] << std::endl;
    }
};

int main()
{
    std::ios::sync_with_stdio(false);

    CnotationConverter notationConverter;
    while (notationConverter.readInput() == true)
    {
        notationConverter.convert();
    }

    return 0;
}