#include <iostream>

void readTriangle(int n, int firstRowSize, int **(&triangle))
{
    triangle = new int *[n];
    for (int row = 0; row < n; ++row)
    {
        triangle[row] = new int[firstRowSize - row];
        for (int character = row; character < firstRowSize - row; ++character)
        {
            char input;
            std::cin >> input;
            if (input == '#')
            {
                triangle[row][character] = 0;
            }
            else
            {
                triangle[row][character] = 1;
            }
        }
    }
}

//if offset == 1 -determine level upwards, if -1 -determine level downwards
int determineLevel(int firstRowSize, int row, int character, int **triangle, int offset)
{
    //check if triangle is valid, if not then its level is 0
    if (triangle[row][character] == 0)
    {
        return 0;
    }

    //check bounds while determining level downwards
    if (offset == -1)
    {
        if (character + 1 >= firstRowSize - row - 1 || character - 1 <= row)
        {
            return 1;
        }
    }

    //check upper middle, if 0 then return 1
    if (triangle[row - 1 * offset][character] == 0)
    {
        return 1;
    }

    //check upper/lower sides
    int minimalLevel = std::min(triangle[row - 1 * offset][character - 1], triangle[row - 1 * offset][character + 1]);
    if (minimalLevel == 0)
    {
        return 1;
    }

    //add level according to previous levels
    return ++minimalLevel;
}

int maxTriangleLevel(int n, int firstRowSize, int **triangle)
{
    /*our optimmization of skipping first/last row will fail the computation if triangle consists of only single triangle
    but we can determine that outright*/
    if (n == 1)
    {
        if (triangle[0][0] == 1)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    int max = 0;
    //start one row from the top, because first row is already "computed" and is not going to change
    for (int row = 1; row < n; ++row) //1
    {
        //for every odd triangle
        for (int character = row; character < firstRowSize - row; character += 2)
        {
            int level = determineLevel(firstRowSize, row, character, triangle, 1);
            triangle[row][character] = level;
            if (level > max)
            {
                max = level;
            }
        }
    }

    //start at pneultimate row, because the last one is already computed
    for (int row = n - 2; row >= 0; --row)
    {
        //for every even triangle
        for (int character = row + 1; character < firstRowSize - row; character += 2)
        {
            int level = determineLevel(firstRowSize, row, character, triangle, -1);
            triangle[row][character] = level;
            if (level > max)
            {
                max = level;
            }
        }
    }

    return max;
}

int levelToVolume(int level)
{
    int first = 1;
    int last = 2 * level - 1;

    return level * ((first + last) / 2);
}

void deleteTriangle(int n, int **triangle)
{
    for (int row = 0; row < n; ++row)
    {
        delete[] triangle[row];
    }
    delete[] triangle;
}

int main()
{
    std::string row;
    int triangleCounter = 1;
    while (true)
    {
        int n;
        std::cin >> n;
        if (n == 0)
        {
            break;
        }
        else
        {
            int firstRowSize = n * 2 - 1;

            int **triangle;
            readTriangle(n, firstRowSize, triangle);

            std::cout << "Triangle #" << triangleCounter << '\n';
            std::cout << "The largest triangle area is " << levelToVolume(maxTriangleLevel(n, firstRowSize, triangle)) << ".\n"
                      << std::endl;

            deleteTriangle(n, triangle);

            ++triangleCounter;
        }
    }

    return 0;
}
