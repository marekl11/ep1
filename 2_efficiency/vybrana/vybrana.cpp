#include <iostream>
#include <vector>
#include <algorithm>

int main()
{

    int N;
    std::cin >> N;

    while (N != 0)
    {
        std::vector<int> arr;
        while (true)
        {
            int a;
            std::cin >> a;
            if (a == 0)
            {
                std::cout << arr.size() << std::endl;
                --N;
                break;
            }
            auto it = std::upper_bound(arr.begin(), arr.end(), a);
            if (it == arr.end())
            {
                arr.emplace_back(a);
            }
            else
            {
                *it = a;
            }
        }
    }

    return 0;
}